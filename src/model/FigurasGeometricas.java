package model;

public class FigurasGeometricas {
	
	private String nome;	
	private int quantidaDeLados;
	private int area;
	private	int ponto;
	private int reta;
	private int plano;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getQuantidaDeLados() {
		return quantidaDeLados;
	}
	public void setQuantidaDeLados(int quantidaDeLados) {
		this.quantidaDeLados = quantidaDeLados;
	}
	public int getArea() {
		return area;
	}
	public void setArea(int area) {
		this.area = area;
	}
	public int getPonto() {
		return ponto;
	}
	public void setPonto(int ponto) {
		this.ponto = ponto;
	}
	public int getReta() {
		return reta;
	}
	public void setReta(int reta) {
		this.reta = reta;
	}
	public int getPlano() {
		return plano;
	}
	public void setPlano(int plano) {
		this.plano = plano;
	}
	
	

}
